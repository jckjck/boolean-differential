package dataIO;

import static org.junit.Assert.*;
import graphRoutines.Graph;
import graphRoutines.Node;

import java.io.File;

import org.junit.Test;



public class PersistenceUtilitiesTest {
	
	private Graph graph;
	private Node node1;
	private Node node2;

	private void initLocalObjects(){
		this.graph = new Graph(20);
		this.node1 = new Node("first", 1, 1); //$NON-NLS-1$
		this.node2 = new Node("second", 2, 2); //$NON-NLS-1$
		this.graph.addNode(this.node1);
		this.graph.addNode(this.node2);
	}

	@SuppressWarnings("static-method")
	@Test
	public void testSaveStringToFile() {
		String saveString = "first line\n" + "second line\n"; //$NON-NLS-1$ //$NON-NLS-2$
		
		File testFile = new File("testFile.txt"); //$NON-NLS-1$
		testFile.delete();
		assertFalse("File should have been deleted", testFile.exists()); //$NON-NLS-1$
		
		assertTrue("File should have bee saved",  //$NON-NLS-1$
				PersitenceUtilities.saveStringToFile("testFile.txt", saveString)); //$NON-NLS-1$
		
		String newString = PersitenceUtilities.getStringFromFile("testFile.txt"); //$NON-NLS-1$
		assertTrue("saved and got strings should be equal", saveString.equals(newString)); //$NON-NLS-1$
		
		assertFalse("File should not exist",  //$NON-NLS-1$
				PersitenceUtilities.saveStringToFile("noDir/noFile.txt", saveString)); //$NON-NLS-1$
		
		String emptyString = PersitenceUtilities.getStringFromFile("badFile.txt"); //$NON-NLS-1$
		assertTrue("String should be empty", emptyString.length() == 0); //$NON-NLS-1$
	}
	
	
	@Test
	public void testSaveToSerialFile() {
		initLocalObjects();
		
		File testFile = new File("testFile.ser"); //$NON-NLS-1$
		testFile.delete();
		assertFalse("File should have been deleted", testFile.exists()); //$NON-NLS-1$
		
		assertTrue("File should have been saved",  //$NON-NLS-1$
				PersitenceUtilities.saveToSerialFile("testFile.ser", this.graph)); //$NON-NLS-1$
		
		Graph newGraph = PersitenceUtilities.getFromSerialFile("testFile.ser"); //$NON-NLS-1$
		assertEquals(2, newGraph.getSize());
		assertEquals("first", newGraph.getNode(0).getName()); //$NON-NLS-1$
		assertEquals("second", newGraph.getNode(1).getName()); //$NON-NLS-1$
		
		assertFalse("File should not exist",  //$NON-NLS-1$
				PersitenceUtilities.saveToSerialFile("noDir/noFile.txt", newGraph)); //$NON-NLS-1$
		
		Graph secondGraph = PersitenceUtilities.getFromSerialFile("badFile.txt"); //$NON-NLS-1$
		assertTrue("String should be empty", secondGraph.getSize() == 0); //$NON-NLS-1$
	}
	

}
