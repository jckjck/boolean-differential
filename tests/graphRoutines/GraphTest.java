package graphRoutines;

import static org.junit.Assert.*;

import org.junit.Test;

public class GraphTest {

	private Graph graph;
	private Node node1;
	private Node node2;

	private void initLocalObjects(){
		this.graph = new Graph(20);
		this.node1 = new Node("first", 1, 1); //$NON-NLS-1$
		this.node2 = new Node("second", 2, 2); //$NON-NLS-1$
	}
	
	@SuppressWarnings({ "unused", "static-method" })
	@Test (expected = OutOfMemoryError.class )
	public void testGraphConstructorWithParameters() {
		Graph grp = new Graph(1000000000);
	}

	@Test
	public void testAddNode() {
		initLocalObjects();
		
		assertTrue("graph size should be 0", this.graph.getSize() == 0); //$NON-NLS-1$
		
		assertTrue("Should be true", this.graph.addNode(this.node1)); //$NON-NLS-1$
		assertTrue("Should be true", this.graph.addNode(this.node2)); //$NON-NLS-1$
	}
	
	@Test
	public void testGetSize() {
		initLocalObjects();
		
		assertTrue("Graph size should be 0", this.graph.getSize() == 0); //$NON-NLS-1$
		
		this.graph.addNode(this.node1);
		assertTrue("Graph size should be 1", this.graph.getSize() == 1); //$NON-NLS-1$
	}
	
	@Test
	public void testGetNode() {
		initLocalObjects();
		
		this.graph.addNode(this.node1);
		this.graph.addNode(this.node2);

		assertEquals("first", this.graph.getNode(0).getName()); //$NON-NLS-1$
		assertEquals("second", this.graph.getNode(1).getName()); //$NON-NLS-1$
	}
	
	@Test
	public void testGetNodeIndexOutOfBounds() {
		initLocalObjects();
		
		assertNull(this.graph.getNode(-1));
		assertNull(this.graph.getNode(3));
	}
//
//	@Test
//	public void testRemoveNode() {
//		fail("Not yet implemented");
//	}

}
