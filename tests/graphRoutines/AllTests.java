package graphRoutines;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import dataIO.PersistenceUtilitiesTest;

@RunWith(Suite.class)
@SuiteClasses({ NodeTest.class, GraphTest.class, PersistenceUtilitiesTest.class })
public class AllTests {
	// Here all the test will run
}
