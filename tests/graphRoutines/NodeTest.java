package graphRoutines;

import static org.junit.Assert.*;

import org.junit.Test;

public class NodeTest {
	
	private Node node;

	@Test
	public void testSetName() {
		this.node = new Node();
		this.node.setName("testNode"); //$NON-NLS-1$
		assertEquals("testNode", this.node.getName()); //$NON-NLS-1$
	}
	
	@Test
	public void testGetName() {
		this.node = new Node();
		this.node.setName(""); //$NON-NLS-1$
		assertEquals(null, this.node.getName());
	}

	@Test
	public void testToggleInverted() {
		this.node = new Node();
		assertFalse(this.node.isInverted());
		
		this.node.toggleInverted();
		assertTrue(this.node.isInverted());
	}

	@Test
	public void testSetGetRightSuccessor() {
		this.node = new Node();
		this.node.setRightSuccessor(1);
		
		assertEquals(1, this.node.getRightSuccessor());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSetRightSuccessorWithIllegalParam() {
		this.node = new Node();
		this.node.setRightSuccessor(-5);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testSetDownSuccessorWithIllegalParam() {
		this.node = new Node();
		this.node.setDownSuccessor(-5);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testNodeNameNull() {
		this.node = new Node(null, 3, 4);
	}
		
	@Test (expected = IllegalArgumentException.class)
	public void testNodeNameEmpy() {
		this.node = new Node("", 3, 4); //$NON-NLS-1$
	}
	
	@Test
	public void testNode() {
		this.node = new Node("testName", 3, 4); //$NON-NLS-1$
		assertEquals("testName", this.node.getName()); //$NON-NLS-1$
		//assertEquals(3, node.get )
		// TODO: Create exhaustive testing for both constructors
	}
	
	@Test
	public void testToString() {
		this.node = new Node("testName", 3, 4); //$NON-NLS-1$

		String testString = Integer.toString(this.node.getNodeNumber()) + 
				": " + this.node.getName() + ", var: " + this.node.getVarNumber(); //$NON-NLS-1$ //$NON-NLS-2$
		
		assertEquals(testString, this.node.toString());
	}


}
