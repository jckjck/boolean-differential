package dataIO;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import circuitRoutines.Circuit;
import circuitRoutines.CircuitParameters;
import testRoutines.InvalidTestFileException;
import testRoutines.VectorSet;

public class TestFileParser extends FileParser {

	private Path path;
	private final String EXTENSION = ".tst";
	private VectorSet vectorSet;
	
	
	public TestFileParser(String fileName) {
		super(fileName);
		setPath(Paths.get(System.getProperty("file.separator") + getFileName() + EXTENSION));
	}

	
	public VectorSet readVectorSetFromFile() throws InvalidTestFileException {
		openFileForReading(getPath(), getCharset());
		return null;
	}
	
	
	private void initCircuitAndAddParameters() throws InvalidTestFileException, IOException {
		if(getReader() == null)
			System.out.println("Unable to read tests file " + path.toString());
		this.vectorSet = new VectorSet(getVectorCount());
	}
	

	private int getVectorCount() throws IOException {
		if(!findBlockInFile(".VECTORS", getReader()))
			throw new InvalidTestFileException("No vector count present in test file");
		String[] words = getReader().readLine().split(" ");
		return Integer.parseInt(words[1]);
	}


	public Path getPath() {
		return path;
	}


	public void setPath(Path path) {
		this.path = path;
	}


}
