package dataIO;

import helper.FileType;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import testRoutines.InvalidTestFileException;
import testRoutines.VectorSet;
import circuitRoutines.Circuit;

public class FileResourcesAccessor {
	private String fileName = "";
	private String fileDir;
	private FileType fileType;
	private GraphFileParser graphParser;
	private TestFileParser testParser;
	
	
	FileResourcesAccessor(String fileName){
		this.fileName = fileName;
	}	
	
	
	public Circuit getCircuitModel() {
		graphParser = new GraphFileParser(fileName);
		Circuit circuit = null;
		try {
			circuit = graphParser.readCircuitFromFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return circuit;
	}	
	
	
	public VectorSet getVectorSet() {
		testParser = new TestFileParser(fileName);
		VectorSet vectorSet = null;
		try {
			vectorSet = testParser.readVectorSetFromFile();
		} catch (InvalidTestFileException e) {
			e.printStackTrace();
		}
		return vectorSet;
	}	
	
	
	private String getExtension(String fileName){	
		int i = fileName.lastIndexOf('.');		
		if (i > 0) {
		    return fileName.substring(i+1);
		}
		else
			return null;
	}
	
	
	@SuppressWarnings("unused")
	private boolean getFileType() {
		String name = getExtension(fileName);
		
		switch(name) {
			case "grp":
				fileType = FileType.GRAPHFILE;
				fileDir = "graphfiles";
				return true;
			case "tst":
				fileType = FileType.TESTFILE;
				fileDir = "testfiles";
				return true;
			default:
				return false;
		}
	}
	
	
	
	public FileReader Parse(){
		
		
		return null;
		
	}
}
