package dataIO;

import graphRoutines.Graph;
import graphRoutines.Node;
import helper.ElementBase;
import helper.ElementGroup;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import circuitRoutines.Circuit;
import circuitRoutines.CircuitParameters;

public class GraphFileParser extends FileParser {
	
	private Path path;
	private final String EXTENSION = ".agm";
	private Circuit circuit;

	
	public GraphFileParser(String fileName) {
		super(fileName);
		path = Paths.get(System.getProperty("file.separator") + getFileName() + EXTENSION);
	}
	
	
	public Circuit readCircuitFromFile() throws Exception {
		openFileForReading(path, getCharset());
		initCircuitAndAddParameters();
		if(!parseCircuitData())
			System.out.println("Graph count in parameters and in file doesn't mach");
		return this.circuit;
	}


	private void initCircuitAndAddParameters() throws Exception {
		if(getReader() == null)
			System.out.println("Unable to read graph file " + path.toString());
		this.circuit = new Circuit(getCircuitParameters());
	}

	
	private boolean parseCircuitData() throws Exception {		
		if(!findBlockInFile("VAR#   " + circuit.getInputCount(), getReader()))
			System.out.println("No Graphs in circuit");
		return addGraphsToCircuit();
	}


	private boolean addGraphsToCircuit() throws IOException, Exception {
		int counter = 0;
		String line;
		
		while (((line = getReader().readLine()) != null) && (counter++ < circuit.getGraphCount())) {
			addNextGraphToCircuit(line);
		}		
		return counter == circuit.getGraphCount() ? true : false;
	}


	private void addNextGraphToCircuit(String line) throws IOException, Exception {
		if(line.contains("VAR#")) {
			circuit.addGraph(initGraphAndParseGraphData(line));
		}
	}


	private Graph initGraphAndParseGraphData(String line) throws IOException, Exception {
		Graph graph = new Graph();
		String[] words = line.split(" ");
		graph.setOutputNum(Integer.parseInt(words[1].replace(",", "")));
		readGraphData(graph);
		return graph;
	}
	
	
	private void readGraphData(Graph graph) throws Exception {
		locateAndAddParametersToGraphInstance(graph);
		String line;
		while((line = getReader().readLine()).length() > 0) {			
			graph.addNode(initializeNodeAndInsertData(line));
		}	
	}


	private Node initializeNodeAndInsertData(String line) throws IOException {
		Node n = new Node();
		if(line.startsWith("   "))
			insertNodeValues(n, line);
		return n;
	}


	private void insertNodeValues(Node n, String line) {
			String[] node = line.split(" ");
			n.setVarNumber(Integer.parseInt(node[8]));
			n.setNodeNumber(Integer.parseInt(node[0]));
			n.setDownSuccessor(Integer.parseInt(node[4]));
			n.setRightSuccessor(Integer.parseInt(node[5].replace(")", "")));
			n.setName(node[9].replace("\"",""));
	}


	private void locateAndAddParametersToGraphInstance(Graph graph) throws Exception {
		String[] paramTokens = getReader().readLine().split(" ");
		insertNumericParametersToGraph(graph, paramTokens);
		String[] typeTokens = getReader().readLine().split(" ");
		insertElementTypeToGraph(graph, typeTokens);
	}
	
	
	private boolean isElementTypeLine(String line) {
		return line.startsWith("CELL#");
	}


	private void insertElementTypeToGraph(Graph graph, String[] words) throws Exception {
		if(isElementTypeLine(words[0])) {
			if(ElementBase.getElement(words[1]) != ElementGroup.DEFAULT)
				graph.setType(ElementBase.getElement(words[1]));
			else
				throw new Exception("Illegal ElementGroup");
		}
	}
	
	
	private boolean isParameterLine(String line) {
		return line.startsWith("GRP#");
	}


	private void insertNumericParametersToGraph(Graph graph, String[] words) {
		if(isParameterLine(words[0])) {
			graph.setGraphNum(Integer.parseInt(words[1].replace(":", "")));
			graph.setStartNodeNum(Integer.parseInt(words[4].replace(",", "")));
			graph.setLength(Integer.parseInt(words[7]));
		}
	}	


	private CircuitParameters getCircuitParameters() throws IOException {
		findBlockInFile("STAT#", getReader());
		String[] words = getReader().readLine().split(" ");
		return initAndEvaluateParameters(words);
	}


	private CircuitParameters initAndEvaluateParameters(String[] words) {
		CircuitParameters parameters = new CircuitParameters();
		parameters.setNodeCount(Integer.parseInt(words[1]));
		parameters.setVariableCount(Integer.parseInt(words[3]));
		parameters.setGraphCount(Integer.parseInt(words[5]));
		parameters.setInputCount(Integer.parseInt(words[7]));
		parameters.setConstCount(Integer.parseInt(words[9]));
		parameters.setOutputCount(Integer.parseInt(words[11]));
		return parameters;
	}	
}
