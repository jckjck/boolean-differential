package dataIO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class FileParser {
	
	private BufferedReader reader;
	private BufferedWriter writer;
	private String fileName;
	private final Charset CHARSET = Charset.forName("ISO-8859-1");
	private final int MAXLINELENGTH = 256;


	public FileParser(String fileName) {
		this.setFileName(fileName);
	}


	protected void openFileForReading(Path path, Charset charset) {
		try (BufferedReader reader = Files.newBufferedReader(path, charset)) {
			this.reader = reader;	          	
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
		} 
	}
	
	
	protected void openFileForWriting(Path path, Charset charset) throws IOException {
		try (BufferedWriter writer = Files.newBufferedWriter(path, charset)) {
			this.writer = writer;	          	
		} 
		catch (IOException e) {
			throw e;
		}	    
	}
	
	
	protected boolean findBlockInFile(String word, BufferedReader reader) throws IOException {		
		String line;
		
		while((line = reader.readLine()) != null) {
			reader.mark(MAXLINELENGTH);
			if(line.contains(word)) {
				reader.reset();
				return true;
			}				
		}
		return false;
	}


	protected String getFileName() {
		return fileName;
	}


	private void setFileName(String fileName) {
		this.fileName = fileName;
	}


	protected Charset getCharset() {
		return CHARSET;
	}
	
	
	protected BufferedReader getReader() {
		return reader;
	}


	protected void setReader(BufferedReader reader) {
		this.reader = reader;
	}


	protected BufferedWriter getWriter() {
		return writer;
	}


	protected void setWriter(BufferedWriter writer) {
		this.writer = writer;
	}
	
	protected int getMaxLength() {
		return MAXLINELENGTH;
	}

}
