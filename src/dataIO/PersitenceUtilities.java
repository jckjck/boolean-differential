package dataIO;

import graphRoutines.Graph;

import java.io.*;

// TODO: Use file API to read all lines at once, when necessary

public class PersitenceUtilities {

	public static boolean saveStringToFile(String fileName, String saveString) {
		boolean saved = false;
		
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
			try	{
				bw.write(saveString);
				saved = true;
			}
			catch(IOException e) {
				System.out.println("Undefined IOException in opening file:"); //$NON-NLS-1$
				System.out.print("\t"); //$NON-NLS-1$
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		catch(IOException e) {
			System.out.println("Undefined Exception in opening file:"); //$NON-NLS-1$
			System.out.print("\t"); //$NON-NLS-1$
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return saved;
	}

	public static String getStringFromFile(String fileName) {
		StringBuilder sb = new StringBuilder();
		
		try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {			
			try	{
				String s;
				while ((s = br.readLine()) != null) {
					sb.append(s);
					sb.append("\n"); //$NON-NLS-1$
				}
			}
			catch(IOException e) {
				System.out.println("Undefined IOException in opening file:"); //$NON-NLS-1$
				System.out.print("\t"); //$NON-NLS-1$
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		catch(IOException e)
		{
			System.out.println("Undefined IOException in opening file:"); //$NON-NLS-1$
			System.out.print("\t"); //$NON-NLS-1$
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static boolean saveToSerialFile(String fileName, Graph g) {
		boolean saved = false;
		
		try(ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)))) {
			try {
				oos.writeObject(g);
				saved = true;
			}
			catch(IOException e) {
				System.out.println("Undefined IOException in opening file:"); //$NON-NLS-1$
				System.out.print("\t"); //$NON-NLS-1$
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		} // Write multiple catches
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return saved;
	}

	public static Graph getFromSerialFile(String fileName) {
		Graph g = new Graph();
		
		try(ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(fileName)))) {
			try {
				Object obj = ois.readObject();
				if(obj instanceof Graph ) {
					g = (Graph) obj;
				}
			}
			catch(IOException e) {
				System.out.println("Undefined IOException in opening file:"); //$NON-NLS-1$
				System.out.print("\t"); //$NON-NLS-1$
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		} // Catch multiple exceptions
		catch(Exception e) {
			e.printStackTrace();
		}
		return g;
	}

}
