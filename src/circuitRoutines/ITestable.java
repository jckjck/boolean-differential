package circuitRoutines;

public interface ITestable {
	
	public Boolean testElement(Element elementUnderTest);
	
	

}
