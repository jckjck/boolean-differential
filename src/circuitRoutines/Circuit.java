package circuitRoutines;

import graphRoutines.Graph;

import java.util.ArrayList;

public class Circuit {
	@SuppressWarnings("unused")
	private ArrayList<Graph> circuit;
	private CircuitParameters parameters;
	
	
	public Circuit() throws OutOfMemoryError {
		try	{
			this.circuit = new ArrayList<>(10);
		}
		catch(OutOfMemoryError e) {
			throw e;
		}
	}
	
	public Circuit(int size) throws OutOfMemoryError {
		if(size < 0) {
			System.out.println("Size cannot be less than 0. Default size (10) created"); //$NON-NLS-1$
			this.circuit = new ArrayList<>(10);
		}
			
		try {
			this.circuit = new ArrayList<>(size);
		}
		catch(OutOfMemoryError e) {
			throw e;
		}
	}
	
	
	public Circuit(CircuitParameters params) throws OutOfMemoryError {
		if(params.getGraphCount() <= 0) {
			System.out.println("Graph count cannot be 0 or negative. Default size (10) created"); //$NON-NLS-1$
			this.circuit = new ArrayList<>(10);
		}
		try {
			this.circuit = new ArrayList<>(params.getGraphCount());
		}
		catch(OutOfMemoryError e) {
			throw e;
		}
	}
	
	
	public void setParameters(CircuitParameters parameters) {
		this.parameters = parameters;
	}
	
	
	public int getNodeCount() {
		return parameters.getNodeCount();
	}
	
	
	public int getVariableCount() {
		return parameters.getVariableCount();
	}
	
	
	public int getGraphCount() {
		return parameters.getGraphCount();
	}
	
	
	public int getInputCount() {
		return parameters.getInputCount();
	}
	
	
	public int getOutputCount() {
		return parameters.getOutputCount();
	}

	public void addGraph(Graph graph) {
		// TODO Auto-generated method stub
		
	}
}
