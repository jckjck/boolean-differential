package circuitRoutines;

import helper.ElementBase;

public abstract class Element {
	// TODO: Implement abstract class
	protected ElementBase elementClass;
	private int[] testValues;
	private int[] faultValues;
	private int output;
	
	
	public Element(int inputCount) {
		this.testValues = new int[inputCount];
		this.faultValues = new int[inputCount];
		this.output = -1;
	}


	public int getTestValueOfInput(int position) {
		try {
			return this.testValues[position];
		}
		catch(IndexOutOfBoundsException e) {
			System.out.println("Wrong index as getTestValueOfInput parameter"); //$NON-NLS-1$
			System.out.print("\t"); //$NON-NLS-1$
			System.out.println(e.getMessage());
			return -1;
		}
	}
	
	
	public int getFaultValueOfInput(int position) {
		try {
			return this.faultValues[position];
		}
		catch(IndexOutOfBoundsException e) {
			System.out.println("Wrong index as getFaultValueOfInput parameter"); //$NON-NLS-1$
			System.out.print("\t"); //$NON-NLS-1$
			System.out.println(e.getMessage());
			return -1;
		}
	}
	
	
	public ElementBase getElementClass() {
		return this.elementClass;
	}


	public void setTestValueOfInput(int position, int value) {
		this.testValues[position] = value;
	}
	
	
	public void setFaultValueOfInput(int position, int value) {
		this.faultValues[position] = value;
	}


	public int getOutput() {
		return this.output;
	}


	public void setOutput(int value) {
		this.output = value;
	}
}
