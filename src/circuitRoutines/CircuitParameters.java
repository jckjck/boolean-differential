package circuitRoutines;

public final class CircuitParameters {
	private int nodeCount;
	private int variableCount;
	private int graphCount;
	private int inputCount;
	private int constCount;
	private int outCount;
	
	
	public CircuitParameters() {
		nodeCount = 0;
		variableCount = 0;
		graphCount = 0;
		inputCount = 0;
		constCount = 0;
		outCount = 0;
	}
	

	public int getNodeCount() {
		return nodeCount;
	}

	public void setNodeCount(int nodeCount) {
		this.nodeCount = (nodeCount > 0) ? nodeCount : -1;
	}

	public int getVariableCount() {
		return variableCount;
	}

	public void setVariableCount(int variableCount) {
		this.variableCount = (variableCount > 0) ? variableCount : -1;
	}

	public int getGraphCount() {
		return graphCount;
	}

	public void setGraphCount(int graphCount) {
		this.graphCount = (graphCount > 0) ? graphCount : -1;
	}

	public int getInputCount() {
		return inputCount;
	}

	public void setInputCount(int inputCount) {
		this.inputCount = (inputCount > 0) ? inputCount : -1;
	}

	public int getConstCount() {
		return constCount;
	}

	public void setConstCount(int constCount) {
		this.constCount = constCount;
	}

	public int getOutputCount() {
		return outCount;
	}

	public void setOutputCount(int outCount) {
		this.outCount = (outCount > 0) ? outCount : -1;
	}
	
}
