package testRoutines;

public class InvalidTestFileException extends RuntimeException {

	private static final long serialVersionUID = 6971486144960447284L;
	private String message = null;
	
	
	public InvalidTestFileException() {
		super();
	}
	
	
	public InvalidTestFileException(String message) {
		super(message);
		this.message = message;
	}
	
	
	public InvalidTestFileException(Throwable cause) {
		super(cause);
	}

	
	@Override
	public String toString() {
		return message;
	}
	
	
	@Override
	public String getMessage() {
		return message;
	}
}
