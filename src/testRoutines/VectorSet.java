package testRoutines;

public class VectorSet {
	
	private int vectorCount;
	
	
	public VectorSet(int vectorCount) {
		this.vectorCount = vectorCount; 
	}
	

	public int getVectorCount() {
		return vectorCount;
	}
	

	public void setVectorCount(int vectorCount) {
		this.vectorCount = vectorCount;
	}
	
	

}
