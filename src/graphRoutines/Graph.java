
package graphRoutines;

/**
 * @author Jaak K�usaar
 *
 */
import helper.ElementBase;
import helper.ElementGroup;

import java.io.Serializable;
import java.util.*;

public class Graph implements Serializable {
	
	private static final long serialVersionUID = 8058749174538904273L;
	private ArrayList<Node> graph;
	private int graphNum;
	private int startNodeNum;
	private int length;
	private ElementGroup type;
	private int outputNum;
	
	
	/**
	 * Constructors
	 */
	public Graph() throws OutOfMemoryError
	{
		try	{
			this.graph = new ArrayList<>(1);
		}
		catch(OutOfMemoryError e) {
			throw e;
		}
	}
	
	public Graph(int size) throws OutOfMemoryError
	{
		if(size < 0) {
			System.out.println("Size cannot be less than 0. Default size (1) created"); //$NON-NLS-1$
			this.graph = new ArrayList<>(1);
		}
			
		try	{
			this.graph = new ArrayList<>(size);
		}
		catch(OutOfMemoryError e) {
			throw e;
		}
	}
	
	/**
	 * Public Methods
	 */
	public boolean addNode(Node node)
	{
		if(node == null) {
			System.out.println("Cannot add 'null' as node");
			return false;
		}
		try	{
			this.graph.add(node);
		}
		catch(IllegalArgumentException e){
			System.out.println("Failed to add node because of illegal parameter in Node Constructor:"); //$NON-NLS-1$
			System.out.print("\t"); //$NON-NLS-1$
			System.out.println(e.getMessage());
			return false;
		}
		catch(OutOfMemoryError e){
			System.out.println("Failed to allocate memory for node:"); //$NON-NLS-1$
			System.out.print("\t"); //$NON-NLS-1$
			System.out.println(e.getMessage());
			return false;
		}
		catch(Exception e){
			System.out.println("Undefined Exception in Graph.addNode Method:"); //$NON-NLS-1$
			System.out.print("\t"); //$NON-NLS-1$
			System.out.println(e.getMessage());
			return false;
		}
		return true;	
	}
	
	public static boolean removeNode()
	{
		return true;
	}

	public int getSize(){
		return this.graph.size();
	}
	
	public Node getNode(int number) {
		try {
			return this.graph.get(number);
		}
		catch(IndexOutOfBoundsException e) {
			System.out.println("Wrong index as getNode parameter"); //$NON-NLS-1$
			System.out.print("\t"); //$NON-NLS-1$
			System.out.println(e.getMessage());
			return null;
		}
	}

	public int getGraphNum() {
		return graphNum;
	}

	public void setGraphNum(int graphNum) {
		this.graphNum = graphNum;
	}

	public int getStartNodeNum() {
		return startNodeNum;
	}

	public void setStartNodeNum(int startNodeNum) {
		this.startNodeNum = startNodeNum;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public ElementGroup getType() {
		return type;
	}

	public void setType(ElementGroup type) {
		this.type = type;
	}

	public int getOutputNum() {
		return outputNum;
	}

	public void setOutputNum(int outputNum) {
		this.outputNum = outputNum;
	}
	
}
