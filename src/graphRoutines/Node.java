/**
 * 
 */
package graphRoutines;

import java.io.Serializable;

/**
 * @author Jaak K�usaar
 *
 */
public class Node implements Serializable {	

	private static final long serialVersionUID = -5528592521581300256L;
	private boolean inverted;
	private String nodeName;
	private int nodeNumber;
	private int varNumber;
	private int right;
	private int down;
	
	
	/**
	 *  Constructors
	 */
	public Node()
	{
		setRightSuccessor(-1);
		setDownSuccessor(-1);
		this.nodeName = ""; //$NON-NLS-1$
		setNodeNumber(0);
		setVarNumber(0);
		this.inverted = false;
	}
	
	public Node( String name, int number, int var )
	{
		if(name == null)
			throw new IllegalArgumentException("nodeName Parameter cannot be null"); //$NON-NLS-1$
		else if(name.isEmpty())
			throw new IllegalArgumentException("nodeName Parameter cannot be empty string"); //$NON-NLS-1$
		else 
			this.nodeName = name;
		
		if(number < 0)
			throw new IllegalArgumentException("nodeNumber Parameter cannot be a negative value"); //$NON-NLS-1$
		this.setNodeNumber(number);
		
		if(number < 0)
			throw new IllegalArgumentException("varNumber Parameter cannot be a negative value"); //$NON-NLS-1$
		this.setVarNumber(var);	
		
		setRightSuccessor(-1);
		setDownSuccessor(-1);
		this.inverted = false;
	}
	
	
	/**
	 *  Public Methods
	 */
	public String getName()
	{
		return this.nodeName.equals("") ? null : this.nodeName; //$NON-NLS-1$
	}
	
	public void setName(String s)
	{
		this.nodeName = s;
	}
	
	public boolean isInverted()
	{
		return this.inverted;
	}
	
	public void toggleInverted()
	{
		this.inverted = (this.inverted == true) ? false : true;
	}

	/**
	 * @return the right
	 */
	public int getRightSuccessor() {
		return this.right;
	}

	/**
	 * @param r the right to set
	 */
	public void setRightSuccessor(int r) {
		if(r < -1)
		{
			IllegalArgumentException iae = 
					new IllegalArgumentException("setRightSuccessor argument must be greater than -1"); //$NON-NLS-1$
			throw iae;
		}
			
		this.right = r;
	}

	/**
	 * @return the down
	 */
	public int getDownSuccessor() {
		return this.down;
	}

	/**
	 * @param d the down to set
	 */
	public void setDownSuccessor(int d) {
		if(d < -1)
		{
			IllegalArgumentException iae = 
					new IllegalArgumentException("setDownSuccessor argument must be greater than -1"); //$NON-NLS-1$
			throw iae;
		}
			
		this.down = d;
	}

	/**
	 * @return the nodeNumber
	 */
	public int getNodeNumber() {
		return this.nodeNumber;
	}

	/**
	 * @param num the nodeNumber to set
	 */
	public void setNodeNumber(int num) {
		this.nodeNumber = num;
	}

	/**
	 * @return the varNumber
	 */
	public int getVarNumber() {
		return this.varNumber;
	}

	/**
	 * @param var the varNumber to set
	 */
	public void setVarNumber(int var) {
		this.varNumber = var;
	}
	
	@Override
	public String toString() {
		return this.getNodeNumber() + ": " + //$NON-NLS-1$
			   this.getName() + ", var: " + //$NON-NLS-1$
			   this.getVarNumber();
	}
	
}
