package helper;

public enum ElementGroup {
	ANDGROUP,
	ORGROUP,
	ANDORGROUP,
	OUTPUTGROUP,
	NOTGROUP,
	DEFAULT
}
