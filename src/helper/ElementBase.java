package helper;

public enum ElementBase {	
	
	NAND2(ElementGroup.ANDGROUP),
	NAND4(ElementGroup.ANDGROUP),

	NOR2(ElementGroup.ORGROUP),
	NOR4(ElementGroup.ORGROUP),
	
	ANDOR22(ElementGroup.ANDORGROUP),
	
	NOT(ElementGroup.NOTGROUP),
	
	OUT(ElementGroup.OUTPUTGROUP),
	
	DEFAULT;
	
	
	private ElementGroup group;
	
	ElementBase(){};
	
	ElementBase(ElementGroup eg) {
		this.group = eg;
	}
	
	
	@SuppressWarnings("unused")
	private boolean isInElementGroup(ElementGroup eg) {
		return this.group == eg;
	}
	
	
	private static String trimGTECHLibraryName(String elementName) {
		return elementName.replace("GTECH_", "");
	}
	
	
	public static ElementGroup getElement(String elementName) {
		switch(trimGTECHLibraryName(elementName)) {
			case "NAND2":
			case "NAND4":
				return ElementBase.NAND2.getGroup();
			case "NOR2":
			case "NOR4":
				return ElementBase.NOR2.getGroup();
			case "ANDOR22":
				return ElementBase.ANDOR22.getGroup();
			case "NOT":
				return ElementBase.NOT.getGroup();
			case "OUT":
				return ElementBase.OUT.getGroup();
			default:
				return ElementBase.DEFAULT.getGroup();
		}
	}	
	
	
	private ElementGroup getGroup() {
		return this.group;
	}


	
}
